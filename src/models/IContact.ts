export interface IContact {
    username: string,
    telephone: string | number,
    address: string,
    id: number,
}