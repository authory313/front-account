import React, {FC, useState} from 'react';
import {Button, Form, Input} from "antd";
import {rules} from "../utils/rules";
import {useTypedSelector} from "../hooks/useTypedSelector";
import {useAppDispatch} from "../hooks/useAppDispatch";

const LoginForm: FC = () => {

    const {login} = useAppDispatch();

    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const {error, isLoading} = useTypedSelector(state => state.auth);

    const submit = () => {
        login(userName, password)
    }

    return (
        <Form
            name="basic"
            labelCol={{span: 8}}
            wrapperCol={{span: 16}}
            initialValues={{remember: true}}
            onFinish={submit}
            autoComplete="off"
        >
            {error && <div>
                {error}
            </div>}
            <Form.Item
                label="Имя"
                name="username"
                rules={[rules.required('Please input your username!')]}
            >
                <Input
                    value={userName}
                    onChange={e => setUserName(e.target.value)}
                />
            </Form.Item>

            <Form.Item
                label="Пароль"
                name="password"
                rules={[rules.required('Please input your password!')]}
            >
                <Input.Password
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    type={password}
                />
            </Form.Item>

            <Form.Item wrapperCol={{offset: 8, span: 16}}>
                <Button type="primary" htmlType="submit" loading={isLoading}>
                    Войти
                </Button>
            </Form.Item>
        </Form>
    );
};

export default LoginForm;