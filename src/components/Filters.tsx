import React, {useEffect, useState} from 'react';
import {Input} from "antd";
import {useAppDispatch} from "../hooks/useAppDispatch";
import useDebounce from "../hooks/useDebounce";

const Filters = () => {
    const [query, setQuery] = useState('');
    const {filterContacts} = useAppDispatch();
    const debouncedValue = useDebounce<string>(query, 300)

    const getFilteredContact = (e: React.ChangeEvent<HTMLInputElement>) => {
        setQuery(e.target.value);
    }

    useEffect(() => {
        filterContacts(query);
    }, [debouncedValue])

    return (
        <div className={'w900 mt-1'}>
            <Input
                value={query}
                onChange={e => getFilteredContact(e)}
                placeholder={'Поиск по имени'}
            />
        </div>
    );
};

export default Filters;