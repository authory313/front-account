import React, {Dispatch, FC} from 'react';
import {Header} from "antd/es/layout/layout";
import {Menu, Row} from "antd";
import {useNavigate} from "react-router-dom";
import data from "../data/common.json";
import {RouteNames} from "../router";
import {useTypedSelector} from "../hooks/useTypedSelector";
import {useAppDispatch} from "../hooks/useAppDispatch";

const NavBar: FC = () => {
    const router = useNavigate();
    const {isAuth, user} = useTypedSelector(state => state.auth);
    const {logOut} = useAppDispatch();

    return (
        <Header>
            <Row justify={"end"}>
                {isAuth
                    ?
                    <>
                        <div style={{color: "white"}}>{user.username}</div>
                        <Menu
                            onClick={() => logOut()}
                            mode={"vertical"}
                            theme={"dark"}
                            selectable={false}
                            items={[...data.menu.private]}
                        />
                    </>
                    :
                    <>
                        <Menu
                            onClick={() => router(RouteNames.LOGIN)}
                            mode={"vertical"}
                            theme={"dark"}
                            selectable={false}
                            items={[...data.menu.public]}
                        />
                    </>
                }
            </Row>
        </Header>
    );
};

export default NavBar;
