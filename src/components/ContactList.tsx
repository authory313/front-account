import React, {FC} from 'react';
import {Layout} from "antd";
import {IContact} from "../models/IContact";
import ContactItem from "./ContactItem";

export interface IContactList {
    items: IContact[],
}

const ContactList: FC<IContactList> = ({items}) => {
    return (
        <Layout className={"w900"}>
            {items && items.map((item, i) => <ContactItem key={item.id} {...item}/>)}
        </Layout>
    );
};

export default ContactList;