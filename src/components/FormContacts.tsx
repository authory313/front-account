import React, {FC, useState} from 'react';
import {Button, Form, Input, Layout} from "antd";
import {rules} from "../utils/rules";
import {Simulate} from "react-dom/test-utils";
import {useAppDispatch} from "../hooks/useAppDispatch";

const FormContacts: FC = () => {

    const [userName, setUserName] = useState<string>('');
    const [telephone, setTelephone] = useState<number | string>('');
    const [address, setAddress] = useState<string>('');

    const {addContact} = useAppDispatch();

    const submit = () => {
        addContact(userName, telephone, address);
        setUserName('');
        setTelephone('');
        setAddress('');
    }

    return (
        <Layout className={'w900'}>
            <div>Добавить новый контакт</div>
            <Form
                onFinish={submit}
            >
                <Form.Item
                    label="Имя"
                    name="username"
                    rules={[rules.required('Please input username!')]}
                >
                    <Input
                        value={userName}
                        onChange={e => setUserName(e.target.value)}
                    />
                </Form.Item>
                <Form.Item
                    label="Телефон"
                    name="telephone"
                    rules={[rules.required('Please input telephone!')]}
                >
                    <Input
                        value={telephone}
                        onChange={e => setTelephone(e.target.value)}
                    />
                </Form.Item>
                <Form.Item
                    label="Адрес"
                    name="address"
                    rules={[rules.required('Please input address!')]}
                >
                    <Input
                        value={address}
                        onChange={e => setAddress(e.target.value)}
                    />
                </Form.Item>
                <Form.Item wrapperCol={{offset: 8, span: 16}}>
                    <Button type="primary" htmlType="submit">
                        Создать
                    </Button>
                </Form.Item>
            </Form>
        </Layout>
    );
};

export default FormContacts;