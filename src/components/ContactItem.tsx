import React, {FC, useState} from 'react';
import {Avatar, Button, Card, Col, Input, Row} from "antd";
import {IContact} from "../models/IContact";
import {useAppDispatch} from "../hooks/useAppDispatch";


const ContactItem: FC<IContact> = ({username, telephone, address, id}) => {
    const [redo, setRedo] = useState(false);
    const [value, setValue] = useState({username, telephone, address, id});
    const {replaceContact, deleteContact} = useAppDispatch();

    const replace = () => {
        setRedo(prev => !prev)
    }

    const saveContact = () => {
        replaceContact(id, value);
        setRedo(prev => !prev)
    }

    const removeContact = () => {
        deleteContact(id);
    }

    return (
        <div className="site-card-wrapper">
            <Col>

                <Card className={'flex'} bordered={true}>
                    <Avatar src={<img src="https://joeschmoe.io/api/v1/random" style={{width: 32}}/>}/>
                    <div>Имя:
                        <Input
                            readOnly={!redo}
                            onChange={e => setValue({...value, username: e.target.value})}
                            value={value.username} bordered={redo}
                        />
                    </div>
                    <div>Телефон:
                        <Input
                            readOnly={!redo}
                            onChange={e => setValue({...value, telephone: e.target.value})}
                            value={value.telephone} bordered={redo}
                        />
                    </div>
                    <div>Адрес:
                        <Input
                            readOnly={!redo}
                            onChange={e => setValue({...value, address: e.target.value})}
                            value={value.address} bordered={redo}
                        />
                    </div>
                    {
                        redo
                            ?
                            <Button onClick={() => saveContact()}
                                    className={'mr-1'}
                                    type={"primary"}>Сохранить</Button>
                            :
                            <Button
                                className={'mr-1'} onClick={() => replace()}
                                type={"primary"}>Редактировать</Button>
                    }
                    <Button onClick={() => removeContact()} type={"dashed"}>Удалить</Button>
                </Card>
            </Col>
        </div>
    );
};

export default ContactItem;