import React from 'react';
import {Navigate, Route, Routes} from "react-router-dom";
import {privateRoutes, publicRoutes, RouteNames} from "../router";
import {useTypedSelector} from "../hooks/useTypedSelector";

const AppRouter = () => {

    const {isAuth} = useTypedSelector(state => state.auth)

    return (
        isAuth ?
            <Routes>
                {privateRoutes.map(route =>
                    <Route
                        key={`route-${route.path}`}
                        element={<route.component/>}
                        path={route.path}/>
                )}
                <Route path={"*"} element={<Navigate to={RouteNames.ACCOUNT} replace />}/>
            </Routes>

            :

            <Routes>
                {publicRoutes.map(route =>
                    <Route
                        key={`route-${route.path}`}
                        element={<route.component/>}
                        path={route.path}/>
                )}
                <Route path={"*"} element={<Navigate to={RouteNames.LOGIN} replace />}/>
            </Routes>
    );
};

export default AppRouter;