import React from "react";
import Login from "../pages/Login";
import Account from "../pages/Account";

export interface IRoute {
    path: string,
    component: React.ComponentType,

}

export const enum RouteNames {
    LOGIN = "/login",
    ACCOUNT = "/"
}

export const publicRoutes: IRoute[] = [
    {
        path: RouteNames.LOGIN,
        component: Login
    }
];

export const privateRoutes: IRoute[] = [
    {
        path: RouteNames.ACCOUNT,
        component: Account
    }
];