import {IContact} from "../../../models/IContact";

export interface ContactsState {
    contacts: IContact[],
    isLoading: boolean,
    error: string,
}

export enum ContactsActionsEnum {
    FETCH_CONTACTS = "FETCH_CONTACTS",
    SET_ERROR = "SET_ERROR",
    SET_IS_LOADING = "SET_IS_LOADING",
    SET_CONTACTS = "SET_CONTACTS",
    SET_FILTER_CONTACTS = "SET_FILTER_CONTACTS",
    ADD_CONTACT = "ADD_CONTACT",
    REPLACE_CONTACT = "REPLACE_CONTACT",
    DELETE_CONTACT = "DELETE_CONTACT"
}

export interface FetchContacts {
    type: ContactsActionsEnum.FETCH_CONTACTS,
    payload: IContact[]
}

export interface FilterContacts {
    type: ContactsActionsEnum.SET_FILTER_CONTACTS,
    payload: IContact[]
}

export interface SetContactsError {
    type: ContactsActionsEnum.SET_ERROR,
    payload: string
}

export interface SetContacts {
    type: ContactsActionsEnum.SET_CONTACTS,
    payload: IContact[]
}

export interface SetContactsLoading {
    type: ContactsActionsEnum.SET_IS_LOADING,
    payload: boolean,
}

export interface AddContact {
    type: ContactsActionsEnum.ADD_CONTACT,
    payload: IContact,
}

export interface ReplaceContact {
    type: ContactsActionsEnum.REPLACE_CONTACT,
    payload: IContact
}

export interface DeleteContact {
    type: ContactsActionsEnum.DELETE_CONTACT,
    payload: IContact
}

export type ContactsAction =
    FetchContacts |
    SetContactsError |
    SetContactsLoading |
    SetContacts |
    FilterContacts |
    AddContact |
    ReplaceContact |
    DeleteContact
