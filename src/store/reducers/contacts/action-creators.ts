import {IContact} from "../../../models/IContact";
import axios from "axios";
import {AppDispatch} from "../../index";
import {ContactsActionsEnum, SetContacts, SetContactsError, SetContactsLoading} from "./types";

export const ContactsActionCreators = {
    setIsLoadingContacts: (loading: boolean): SetContactsLoading => ({
        type: ContactsActionsEnum.SET_IS_LOADING,
        payload: loading
    }),

    setIsErrorContacts: (error: string): SetContactsError => ({
        type: ContactsActionsEnum.SET_ERROR,
        payload: error
    }),

    setContacts: (contacts: IContact[]): SetContacts => ({
        type: ContactsActionsEnum.SET_CONTACTS,
        payload: contacts
    }),

    fetchContacts: () => async (dispatch: AppDispatch) => {
        try {
            dispatch(ContactsActionCreators.setIsLoadingContacts(true));
            const response = await axios.get<IContact[]>("http://localhost:9000/contacts");
            dispatch(ContactsActionCreators.setContacts(response.data))
            dispatch(ContactsActionCreators.setIsLoadingContacts(false));
        } catch (e) {
            dispatch(ContactsActionCreators.setIsErrorContacts('произошла ошибка'));
        }
    },

    filterContacts: (query: string) => async (dispatch: AppDispatch) => {
        try {
            dispatch(ContactsActionCreators.setIsLoadingContacts(true));
            const response = await axios.get<IContact[]>(`http://localhost:9000/contacts?q=${query}`);
            dispatch(ContactsActionCreators.setContacts(response.data))
            dispatch(ContactsActionCreators.setIsLoadingContacts(false));
        } catch (e) {
            dispatch(ContactsActionCreators.setIsErrorContacts("произошла ошибка"))
        }
    },

    addContact: (username: string, telephone: number | string, address: string) => async (dispatch: AppDispatch) => {
        try {
            await axios.post("http://localhost:9000/contacts/", {
                username,
                telephone,
                address
            }, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            await dispatch(ContactsActionCreators.fetchContacts());
        } catch (e) {
            dispatch(ContactsActionCreators.setIsErrorContacts("произошла ошибка при загрузке контакта"))
        }
    },

    replaceContact: (id: number, data: IContact) => async (dispatch: AppDispatch) => {
        try {
            await axios.patch(`http://localhost:9000/contacts/${id}`, data);
            await dispatch(ContactsActionCreators.fetchContacts());
        } catch (e) {
            dispatch(ContactsActionCreators.setIsErrorContacts("произошла ошибка при редактировании контакта"))
        }
    },

    deleteContact: (id: number) => async (dispatch: AppDispatch) => {
        try {
            await axios.delete(`http://localhost:9000/contacts/${id}`);
            await dispatch(ContactsActionCreators.fetchContacts());
        } catch (e) {
            dispatch(ContactsActionCreators.setIsErrorContacts("произошла ошибка"))
        }
    }

}