import {ContactsAction, ContactsActionsEnum, ContactsState} from "./types";
import {IContact} from "../../../models/IContact";

const initialState : ContactsState = {
    contacts: [] as IContact[],
    error: "",
    isLoading: false
}

export default function contactsReducer(state = initialState, action: ContactsAction): ContactsState {
    switch (action.type) {
        case ContactsActionsEnum.FETCH_CONTACTS:
            return {...state, isLoading: true, error: ''}

        case ContactsActionsEnum.SET_ERROR:
            return {...state, error: action.payload}

        case ContactsActionsEnum.SET_IS_LOADING:
            return {...state, isLoading: action.payload}

        case ContactsActionsEnum.SET_CONTACTS:
            return {...state, contacts: action.payload}

        case ContactsActionsEnum.SET_FILTER_CONTACTS:
            return {...state, contacts: action.payload}

        case ContactsActionsEnum.ADD_CONTACT:
                return {
                    ...state,
                    contacts: [...state.contacts, action.payload]
                }

        case ContactsActionsEnum.REPLACE_CONTACT:
            return {
                ...state,
                contacts: [...state.contacts, action.payload]
            }

        case ContactsActionsEnum.DELETE_CONTACT:
            return {
                ...state,
                contacts: [...state.contacts]
            }


        default:
            return state
    }
}