import {AuthActionsEnum, SetAuthAction, SetErrorAction, SetIsLoadingAction, SetUserAction} from "./types";
import {IUser} from "../../../models/IUser";
import {AppDispatch} from "../../index";
import axios from "axios";

export const AuthActionCreators = {
    setUser: (user: IUser): SetUserAction => ({
        type: AuthActionsEnum.SET_USER,
        payload: user
    }),

    setIsAuth: (auth: boolean): SetAuthAction => ({
        type: AuthActionsEnum.SET_AUTH,
        payload: auth
    }),

    setIsLoading: (loading: boolean): SetIsLoadingAction => ({
        type: AuthActionsEnum.SET_IS_LOADING,
        payload: loading
    }),

    setIsError: (error: string): SetErrorAction => ({
        type: AuthActionsEnum.SET_ERROR,
        payload: error
    }),

    login: (username: string, password: string) => async (dispatch: AppDispatch) => {
        try {
            dispatch(AuthActionCreators.setIsLoading(true));
            const response = await axios.get<IUser[]>("http://localhost:9000/users");
            const mockUser = response.data.find(u => u.username === username && u.password === password);
            if (mockUser) {
                localStorage.setItem("auth", "true");
                localStorage.setItem("username", mockUser.username);
                dispatch(AuthActionCreators.setIsAuth(true));
                dispatch(AuthActionCreators.setUser(mockUser));
            } else {
                dispatch(AuthActionCreators.setIsError("err login or password"));
                dispatch((AuthActionCreators.setIsLoading(false)));
            }
        } catch (e) {
            dispatch(AuthActionCreators.setIsError('error'))
        }
    },

    logOut: () => async (dispatch: AppDispatch) => {
        localStorage.removeItem("auth");
        localStorage.removeItem("username");
        dispatch(AuthActionCreators.setUser({} as IUser));
        dispatch(AuthActionCreators.setIsAuth(false));
    }
}