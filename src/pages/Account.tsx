import React, {FC, useEffect} from 'react';
import {useAppDispatch} from "../hooks/useAppDispatch";
import {useTypedSelector} from "../hooks/useTypedSelector";
import ContactList from "../components/ContactList";
import Filters from "../components/Filters";
import FormContacts from "../components/FormContacts";

const Account: FC = () => {

    const {fetchContacts} = useAppDispatch();

    useEffect(() => {
        fetchContacts()
    }, [])

    const {contacts, isLoading, error} = useTypedSelector(state => state.contacts);

    return (
        <div>
            <Filters/>
            <FormContacts/>
            {isLoading && <h2>Идет загрузка</h2>}
            {!error &&
                <ContactList items={contacts}/>}
        </div>
    );
};

export default Account;