import {useEffect, useState} from "react";

export default function useDebounce<T>(value: T, delay?: number) {
    const [debouncedValue, setDebouncedValue] = useState<T>();

    useEffect(() => {
        const handler = setTimeout(() => {
            setDebouncedValue(value);
        }, delay || 300)

        return () => {
            clearTimeout(handler);
        }
    }, [value])

    return debouncedValue;
}