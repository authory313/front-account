import React, {FC, useEffect} from 'react';
import AppRouter from "./components/AppRouter";
import Layout, {Content} from "antd/es/layout/layout";
import NavBar from "./components/NavBar";
import {useAppDispatch} from "./hooks/useAppDispatch";
import {IUser} from "./models/IUser";

const App: FC = () => {

    const {setUser, setIsAuth} = useAppDispatch();

    useEffect(() => {
        if (localStorage.getItem("auth")) {
            setUser({username: localStorage.getItem("username" || "")} as IUser);
            setIsAuth(true);
        }
    }, [])

    return (
        <Layout>
            <NavBar/>
            <Content>
                <AppRouter/>
            </Content>
        </Layout>
    );
}

export default App;
